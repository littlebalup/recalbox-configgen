# recalbox-configgen

Moved to the monorepo : https://gitlab.com/recalbox/recalbox/-/tree/master/projects/configgen/



##Emulators configuration tools for recalboxOS

*WIP*

The purpose of the lib is to automatically configure emulators, using command line args and environment available in recalbox.

How to test:
```
python -m runtest discover -p "*test*"
```

How to start:
```
python configgen/emulatorlauncher.py  -system neogeo -rom myrom.zip
```
